<?php

namespace NsGoogleUtils\Services;

use Exception;
use Google\Service\Sheets\Sheet;
use AppLibrary\Config;
use Google\Service\Sheets\ValueRange;
use NsUtil\Helper;
use NsUtil\Log;

use function NsUtil\dd;

class Sheets
{
    private string $sheetId;
    private \Google\Service\Sheets $service;
    private $requests = [];
    private $tabName = [];

    public const COLUMNS = [
        'A' => 0,  'B' => 1,  'C' => 2,  'D' => 3,  'E' => 4,
        'F' => 5,  'G' => 6,  'H' => 7,  'I' => 8,  'J' => 9,
        'K' => 10, 'L' => 11, 'M' => 12, 'N' => 13, 'O' => 14,
        'P' => 15, 'Q' => 16, 'R' => 17, 'S' => 18, 'T' => 19,
        'U' => 20, 'V' => 21, 'W' => 22, 'X' => 23, 'Y' => 24,
        'Z' => 25
    ];


    public function __construct(string $sheetId)
    {

        $client = new \Google\Client();
        $client->setApplicationName('Google Sheets API');
        $client->addScope(\Google\Service\Sheets::SPREADSHEETS);
        $client->addScope(\Google\Service\Script::SCRIPT_DEPLOYMENTS);
        $client->addScope(\Google\Service\Script::SCRIPT_PROJECTS);
        $client->setAccessType('offline');
        $path = Helper::getPathApp() . '/private/arca-sheets.json';
        $client->setAuthConfig($path);

        // configure the Sheets Service
        $this->service = new \Google\Service\Sheets($client);

        $this->sheetId = $sheetId;
    }

    public function loadData($range)
    {
        $response = $this->service->spreadsheets_values->get($this->sheetId, $range);
        return $response->getValues();
    }

    public function getTabID($tabName)
    {
        // Obter a ID da aba 
        $response = $this->service->spreadsheets->get($this->sheetId);
        $sheets = $response->getSheets();
        $sheetsId = array_values(array_filter($sheets, fn ($sheet) => $sheet['properties']['title'] === $tabName));
        if (null === ($sheetsId[0] ?? null)) {
            throw new Exception('Tab not found: ' . $tabName . var_export(Log::getBacktrace(), true));
        }
        $this->tabName[$tabName] = $sheetsId[0]['properties']['sheetId'];

        return $this->tabName[$tabName];
    }

    public function requestsExecute(array $request)
    {
        $this->service->spreadsheets->batchUpdate(
            $this->sheetId,
            new \Google\Service\Sheets\BatchUpdateSpreadsheetRequest([
                'requests' => $request,
            ])
        );

        return $this;
    }

    public function updateRow(string $range, array $data)
    {
        $options = ['valueInputOption' => 'USER_ENTERED'];
        $valueRange = new ValueRange();
        $valueRange->setValues($data);
        $this->service->spreadsheets_values->update($this->sheetId, $range, $valueRange, $options);
    }

    public function createTab(string $tabName, bool $removeIfExists = false)
    {
        try {
            $sheetId = $this->getTabID($tabName);
            if ($removeIfExists) {
                $this->requestsExecute([
                    'deleteSheet' => [
                        'sheetId' => $sheetId
                    ]
                ]);
                sleep(1);
                throw new Exception('');
            }
            return $this;
        } catch (Exception $exc) {
            $this->requestsExecute([
                'addSheet' => [
                    'properties' => [
                        'title' => $tabName
                    ]
                ]
            ]);
        }
        return $this;
    }

    public function importCSV($range, array $data)
    {
        $options = ['valueInputOption' => 'USER_ENTERED'];

        // Clear
        $this->service->spreadsheets_values->clear($this->sheetId, $range, new \Google\Service\Sheets\ClearValuesRequest());

        // insert new data
        $valueRange = new \Google\Service\Sheets\ValueRange();
        $valueRange->setValues($data);

        $this->service->spreadsheets_values->append($this->sheetId, $range, $valueRange, $options);

        return $this;
    }

    public function hideTab($tabName)
    {
        $this->requestsExecute([
            'updateSheetProperties' => [
                'properties' => [
                    'sheetId' => $this->getTabID($tabName),
                    'hidden' => true
                ],
                'fields' => 'hidden'
            ]
        ]);

        return $this;
    }

    public function configureRowsHigh($tabName, $rowHeight)
    {
        $this->requestsExecute([
            [
                'updateDimensionProperties' => [
                    'range' => [
                        'sheetId' => $this->getTabID($tabName),
                        'dimension' => 'ROWS',
                        'startIndex' => 0, // Início da primeira linha
                        'endIndex' => 10000, // Fim de um intervalo grande para garantir todas as linhas
                    ],
                    'properties' => [
                        'pixelSize' => $rowHeight, // Altura desejada em pixels
                    ],
                    'fields' => 'pixelSize',
                ],
            ]
        ]);

        return $this;
    }
}
